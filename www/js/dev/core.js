if (!window.jQuery){
  window.alert("JQ Doesn't Work");
}

var summary_counter = 0;

var ncbi = {
  esearch_build_url : function(schedule, retstart = null, mindate = false){

    var terms_string = '';

    for(var i = 0; i < schedule.index_fields.length; i++){
      var field = schedule.index_fields[i].value == 'all' ? '' : '['+schedule.index_fields[i].value+']';

      if(i > 0){
        terms_string += "+"+schedule.connectors[i].value+"+";
      }

      terms_string += schedule.terms[i].value+field;
    }

    var url_complete = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db="+schedule.support_name
    +"&tool=ncbionotifier"
    +"&email=matteusbarbosa2@gmail.com"
    +"&datetype=mdat"
    +"&retmode=json"
    +"&retmax="+CONFIG.SEARCH_RETMAX;

    url_complete += '&reldate='+schedule.days_gap+'&sort='+schedule.sort_date;

    if(retstart != null)
    url_complete += "&retstart="+retstart;

    url_complete += "&term="+terms_string;

    if(schedule.free_only == 1)
    url_complete += "+AND+free+fulltext";

    return url_complete;
  },
  esearch_list_results : function(DB_TARGET, support_name, schedule, esearch_url, set_last_viewed = false, summary_format = null){
    console.log(esearch_url);
    var req_search = $.ajax({
      url: esearch_url
    });
    req_search.fail(function(esearch_result_obj) {
      window.alert( "Inconsistent object atributes");
    });
    req_search.done(function(esearch_result_obj){

      console.log(esearch_result_obj);

      var time = new Date();

      if(typeof esearch_result_obj.esearchresult == 'undefined'){
        console.error("Inconsistent result");
        return false;
      }

      var list_id = esearch_result_obj.esearchresult.idlist.join(',');
      ncbi.set_last_result_viewed(list_id);
      var list_pub_id = esearch_result_obj.esearchresult.idlist;
      var result_counter = parseInt($('#esearch-list-count').text());
      if(result_counter + CONFIG.SEARCH_RETMAX >= esearch_result_obj.esearchresult.count)
      $('#load-next-results').addClass('hide');
      else
      $('#load-next-results').removeClass('hide');
      var result_count = parseInt($('#esearch-list-count').text()) + CONFIG.SEARCH_RETMAX;
      result_count = esearch_result_obj.esearchresult.count > result_count ? result_count : esearch_result_obj.esearchresult.count;

      $('#esearch-list-count').text(result_count);

      $('#esearch-list-ids').append('<h4 class="info-load-number">Displaying <strong><span class="count-results-display">'+result_count+'</span></strong>/'+esearch_result_obj.esearchresult.count+' results.<br>'
      +'• Results from '+schedule.days_gap+' days ago.</h4><hr>');

      for(var i = 0; i < list_pub_id.length; i++){

        var req_summary = $.ajax({
          url: 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db='+schedule.support_name+'&id='+list_pub_id[i]+'&retmode=xml',
          context: {
            url : 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db='+schedule.support_name+'&id='+list_pub_id[i]+'&retmode=xml'
          }
        });

        req_summary.done(function(esummary_result_obj){
          if(!esummary_result_obj.getElementsByTagName("eSummaryResult")){
            throw new Error("Invalid result");
            return;
          }

          console.log(data.url);

          if(i == 0 && set_last_viewed != false){
            set_last_viewed(window.IDB_SCHEDULE, schedule, esearch_result_obj, esummary_result_obj);
          }

          if(summary_format != null){
            summary_format(window.IDB_SCHEDULE, esummary_result_obj);
          }

          result_counter += 1;
        });
      }
    });
  },
  load_next_results: function(DB_TARGET, support_name, set_last_viewed = false, summary_format){

    var title_key = schedule.get_last_viewed();

    var read_transition = DB_TARGET.transaction([support_name], "readonly");
    var objectStore = read_transition.objectStore(support_name);

    var request = objectStore.get(title_key);

    request.onsuccess = function(event) {
      var data = request.result;

      var url = ncbi.esearch_build_url(data, parseInt($('#esearch-list-count').text()), true);

      ncbi.esearch_list_results(DB_TARGET, support_name, data, url, set_last_viewed, summary_format);

    };
  },
  get_notification_count : function(DB_TARGET, support_name, title_key){

    var read_transition = DB_TARGET.transaction([support_name], "readonly");
    var objectStore = read_transition.objectStore(support_name);

    var request = objectStore.get(title_key);

    request.onsuccess = function(event) {
      var data = request.result;

      var esearch_url = ncbi.esearch_build_url(data);

      var req_search = $.ajax({
        url: esearch_url
      });
      req_search.fail(function(esearch_result_obj) {
        return null;
      });
      req_search.done(function(esearch_result_obj){

        if(typeof esearch_result_obj.esearchresult == 'undefined'){
          throw new Error("Inconsistent result");
          return null;
        }

        return esearch_result_obj.esearchresult.count;
      });

    };
  },
  set_last_result_viewed : function(list_id){
    localStorage.setItem("ncbi[last_result_viewed]", list_id);
  },
  get_last_result_viewed : function(){
    return localStorage.getItem("ncbi[last_result_viewed]");
  }
};

if (!window.jQuery)
window.alert("JQ Doesn't Work");

if(typeof schedule != 'undefined')
var schedule = schedule;

if(typeof ncbi != 'undefined')
var ncbi = ncbi;

var notify = {
  new : function(schedule_obj){

    schedule.set_last_viewed(schedule_obj.title_key);

    if(window.IDB_SCHEDULE == null || typeof window.IDB_SCHEDULE == 'undefined')
    window.alert('window.IDB_SCHEDULE PROBLEM');

    var esearch_url = ncbi.esearch_build_url(schedule_obj);

    var req_search = $.ajax({
      url: esearch_url
    });

    req_search.fail(function(esearch_result_obj) {
      return null;
    });

    req_search.done(function(esearch_result_obj){

      if(typeof esearch_result_obj.esearchresult == 'undefined'){
        throw new Error("Inconsistent result");
        return null;
      }

      var result_count_preview = esearch_result_obj.esearchresult.count;

      var first_fire = new Date(schedule_obj.start_at+" "+schedule_obj.time+":00");

      notify.checkPermission();

      cordova.plugins.notification.local.schedule({
        id: schedule_obj.id_key,
        title: schedule_obj.support_name+' – '+schedule_obj.title_key+' ('+result_count_preview+' recent results)',
        text: "Search every "+schedule_obj.every_gap+".",
        at : first_fire,
        every : schedule_obj.every_gap,
        sound: "file://assets/sound/glitchy-language.mp3",
        data: {
          support_name: schedule_obj.support_name,
          title_key: schedule_obj.title_key,
          details: schedule_obj.details,
          time: schedule_obj.time,
          result_count : 0}
        });

      });
    },
    trigger: function(notification){
      schedule.set_last_viewed(notification.data.title_key);

      if(window.IDB_SCHEDULE == null || typeof window.IDB_SCHEDULE == 'undefined')
      window.alert('window.IDB_SCHEDULE PROBLEM');

      var read_transition = window.IDB_SCHEDULE.transaction([notification.data.support_name], "readonly");
      var objectStore = read_transition.objectStore(notification.data.support_name);

      var request = objectStore.get(notification.data.title_key);

      request.onsuccess = function(event) {

        var data = request.result;
        var esearch_url = ncbi.esearch_build_url(data);
        var req_search = $.ajax({
          url: esearch_url
        });

        req_search.fail(function(esearch_result_obj) {
          return null;
        });

        req_search.done(function(esearch_result_obj){

          if(typeof esearch_result_obj.esearchresult == 'undefined'){
            throw new Error("Inconsistent result");
            return null;
          }

          var result_count_preview = esearch_result_obj.esearchresult.count

          cordova.plugins.notification.local.update({
            id: notification.id,
            title: notification.data.title_key+' ('+result_count_preview+' recent results)',
            text: schedule.title_key,
            data: { result_count: result_count_preview }
          });
        });
      };
    },
    checkPermission : function(){
      cordova.plugins.notification.local.registerPermission(function (granted) {
        return granted;
      });
    }
  };

  /*
  REQ-01. Deve agendar consultas em localstorage
  */
  if (!window.jQuery)
  window.alert("JQ Doesn't Work");

  if(typeof notify != 'undefined')
  var notify = notify;

  var schedule = {
    list : function(DB_TARGET, support_name){
      var read_transition = DB_TARGET.transaction([support_name], "readonly");
      var store = read_transition.objectStore(support_name);
      var rows = store.openCursor();
      var result = [];

      rows.onsuccess = function(evt){
        var c = 0;
        var cursor = evt.target.result;
        if(cursor){
          $('#schedule-list > tbody').append('<tr>'
          +'<td><button class="btn btn-default btn-sm btn-key-details-show"><i class="fa fa-search"></i></button><span class="hide title-key">'+cursor.key+'</span></td>'
          +'<td>'+cursor.value.title_key+'</td>'
          +'<td>'+cursor.value.details+'</td>'
          +'<td><a href="#" class="btn-key-remove text-danger"><i class="fa fa-times fa-2x"></i> <span class="hide title-key">'+cursor.key+'</span></a></td>'
          +'</tr>');
          c++;
          cursor.continue();
        }

        if(c > 0){
          $('#alert-schedule-null').addClass('hide');
          $('#schedule-list thead').removeClass('hide');
        }
      }
    },
    show : function(DB_TARGET, support_name, title_key){
      var read_transition = DB_TARGET.transaction([support_name], "readwrite");
      var objectStore = read_transition.objectStore(support_name);
      var request = objectStore.get(title_key);
      request.onsuccess = function(event) {
        var data = request.result;
        schedule.set_last_viewed(data.title_key);
        var esearch_url = ncbi.esearch_build_url(data);
        ncbi.esearch_list_results(DB_TARGET, support_name, data, esearch_url);
        var list_terms = data.terms;
        var list_schedule = data.days_interval;
        var html_body = '<h5 style="color: #000;">Terms: </h5><div class="well well-sm"><ul>';
        for(var c = 0; c < list_terms.length; c++)
        html_body += '<li><span class="label label-success">'+list_terms[c].value+'</span></li>';
        html_body += '</ul></div>';
        html_body += '<h5 style="color: #000;">Schedule: </h5><p>You will be notified in the following days '+data.details+':</p><div class="well well-sm">'
        +'<ul class="list-inline" id="list-schedule-dates">';
        for(var c = 0; c < list_schedule.length;c++){
          html_body += '<li><span class="badge">'+list_schedule[c]+'</span></li>';
        }
        html_body += '</ul></div>';
        html_body += '<h5 style="color: #000;">Results: </h5>';
        if(data.result_last_created_at != null){
          html_body += '<div class="fill-dark-transparent"> Last execution: <br><ul><li>Executed at: '+data.executed_at+'</li><li>Results count: '+data.result_count+'</li><li>Most recent created at: '+data.result_last_created_at+'</li><li>Most recent updated at: '+data.result_last_updated_at+'</li></ul></div>';
        }

        html_body += '<a href="../'+data.support_name+'/esearch.html" class="esearch-list-ids" style="color: red;">Get results now</a>';

        $('.modal-header').find('h4').text(title_key+' ('+support_name+')');
        $('.modal-body').html(html_body);
      };
    },
    get : function(result){
      return result;
    },
    set : function(DB_TARGET, support_name, title_key, terms_array, connectors, index_fields_array, days_gap, time, sort_date, free_only){
      var start_at = new Date();
      var schedule_obj = new Object();

      schedule_obj.support_name = support_name;
      schedule_obj.title_key = title_key;
      schedule_obj.id_key = Math.floor((Math.random() * 100000) + 1);
      terms_array.shift();
      connectors.shift();
      index_fields_array.shift();
      schedule_obj.terms = terms_array;
      schedule_obj.connectors = connectors;
      schedule_obj.index_fields = index_fields_array;
      schedule_obj.sort_date = sort_date;
      schedule_obj.free_only = free_only;
      schedule_obj.days_gap = days_gap;
      schedule_obj.time = time;
      schedule_obj.start_at = start_at.getFullYear()+"/"+(start_at.getMonth()+1)+"/"+start_at.getDate();
      schedule_obj.days_interval = [];
      var c = 0;
      while(c < CONFIG.SCHEDULE_REPEAT_LIMIT){
        schedule_obj.days_interval[c] = start_at.getFullYear()+"/"+(start_at.getMonth()+1)+"/"+start_at.getDate();
        start_at.addDays(days_gap);
        c++;
      }

      var end_at = schedule_obj.days_interval[c];
      schedule_obj.days_interval = schedule_obj.days_interval;

      var every_gap = '';

      switch(days_gap){
        case "1":
        every_gap = 'day';
        break;
        case "7":
        every_gap = 'week';
        break;
        case "30":
        every_gap = 'month';
        break;
      }

      schedule_obj.details = 'at '+time+':00 every '+every_gap;
      schedule_obj.every_gap = every_gap;

      var write_transition = DB_TARGET.transaction([support_name], "readwrite");
      var store = write_transition.objectStore(support_name);
      var objectStoreRequest = store.put(schedule_obj);

      objectStoreRequest.onsuccess = function(evt){
        var schedule = objectStoreRequest.result;
        //later, find the schedule by title_key and build the query url
        var notify_set = notify.new(schedule_obj);

        cordova.plugins.notification.local.cancel(schedule_obj.id_key, function() {
          window.alert("Schedule updated sucessfully.");
        });

      };
    },
    exists : function(term){
      return window.localStorage.getItem(term) || false;
    },
    delete : function(DB_TARGET, support_name, title_key){
      console.log(DB_TARGET);
      console.log(support_name);
      console.log(title_key);
      var delete_transition = DB_TARGET.transaction([support_name], "readwrite");
      // create an object store on the transaction
      var objectStore = delete_transition.objectStore(support_name);
      // Delete the specified record out of the object store
      var objectStoreRequest = objectStore.delete(title_key);

      objectStoreRequest.onsuccess = function(event) {
        var id = new Date(support_name+"-"+title_key);
        cordova.plugins.notification.local.cancel(id, function() {
          window.alert("Notification removed");
        });
      };
    },
    set_last_viewed : function(title_key){
      localStorage.setItem("schedule[last_viewed]", title_key);
    },
    get_last_viewed : function(){
      return localStorage.getItem("schedule[last_viewed]");
    }
  };
