function versioning(){
  if(localStorage.getItem("config.IDB.VERSION_COUNT") != null){
      var r = parseInt(localStorage.getItem("config.IDB.VERSION_COUNT"))+1;
      localStorage.setItem("config.IDB.VERSION_COUNT", r);
  } else {
    var r = 1;
    localStorage.setItem("config.IDB.VERSION_COUNT", 1);
  }
return r;
}

function idb_setup(support_name, store_create){
/*
if(localStorage.getItem("config.IDB."+support_name+".VERSION") == null){
  var version = localStorage.getItem("config.IDB.VERSION_COUNT") != null ? parseInt(localStorage.getItem("config.IDB.VERSION_COUNT"))+1 : 1;
  localStorage.setItem("config.IDB.VERSION_COUNT", version);
  localStorage.setItem("config.IDB."+support_name+".VERSION", version);
} else {
  var version = parseInt(localStorage.getItem("config.IDB."+support_name+".VERSION"));
  localStorage.setItem("config.IDB."+support_name+".VERSION", version);
}*/

var db_open = indexedDB.open("schedule", versioning());

if(typeof IDB_SCHEDULE == 'undefined')
var IDB_SCHEDULE = null;

  db_open.onupgradeneeded = function(event){
    IDB_SCHEDULE = event.target.result;

    var st_names = IDB_SCHEDULE.objectStoreNames;

    if(!st_names.contains(support_name))
    store_create(IDB_SCHEDULE);

  };

db_open.onsuccess = function(e){
    //database connection established
    //IDB_SCHEDULE = request.result;
    window.IDB_SCHEDULE = db_open.result;
};

db_open.onerror = function(){
};

}
