if (!window.jQuery)
window.alert("JQ Doesn't Work");

$(document).ready(function(){
  $('.mask-date').mask('00/00/0000');
  $("#modal-wrap").load("./../layout/_modal.html");
  $("aside#sidebar-top").load("./../layout/_sidebar_top.html");
  $(".form-validate").validationEngine('attach', {promptPosition : "topLeft", scroll: true});
});

  $(document).on('click', '.btn-page-previous', function(e){
  e.preventDefault();
  $('body').prop('id', '').html('<img src="../assets/img/load-logo-splash.png" class="center-block" style="margin-top: 20%">');
  window.setTimeout(function(){
    history.go(-1);
  }, 1000);
});

//ativar em caso de necessidade de entender algum erro
window.onerror = function (errorMsg, url, lineNumber, column, errorObj) {
  //  window.alert('Error: ' + errorMsg + ' Script: ' + url + ' Line: ' + lineNumber + ' Column: ' + column + ' StackTrace: ' +  errorObj);
    console.error('Error: ' + errorMsg + ' Script: ' + url + ' Line: ' + lineNumber + ' Column: ' + column + ' StackTrace: ' +  errorObj);
};
