if (!window.jQuery)
window.alert("JQ Doesn't Work");

if(typeof schedule != 'undefined')
var schedule = schedule;

if(typeof ncbi != 'undefined')
var ncbi = ncbi;

var support_name = "protein";

function store_create(IDB_SCHEDULE){

  var store = IDB_SCHEDULE.createObjectStore("protein", {keyPath: "title_key", autoIncrement: false});

  //define other properties of the objects of that store i.e., define other columns.
  store.createIndex("id", "id", {unique: true, autoIncrement : true});
  store.createIndex("title_key", "title_key", {unique: true});
  store.createIndex("id_key", "id_key", {unique: true});
  store.createIndex("connectors", "connectors", {unique: false});
  store.createIndex("days_gap", "days_gap", {unique: false});
  store.createIndex("every_gap", "every_gap", {unique: false});
  store.createIndex("index_fields", "index_fields", {unique: false});
  store.createIndex("details", "details", {unique: false});
  store.createIndex("free_only", "free_only", {unique: false});
  store.createIndex("time", "time", {unique: false});
  store.createIndex("url", "url", {unique: false});
  store.createIndex("terms", "terms", {unique: false});
  store.createIndex("sort_date", "sort_date", {unique: false});
  store.createIndex("created_at", "created_at", {unique: false});
  store.createIndex("start_at", "start_at", {unique: false});
  store.createIndex("days_interval", "days_interval", {unique: false});
  store.createIndex("result_count", "result_count", {unique: false});
  store.createIndex("executed_at", "executed_at", {unique: false});
  store.createIndex("result_last_created_at", "result_last_created_at", {unique: false});
  store.createIndex("result_last_updated_at", "result_last_updated_at", {unique: false});
  store.createIndex("support_name", "support_name", {unique: false});
};


window.onload = function(){

  idb_setup(support_name, store_create);

  if($('#schedule-list').length == 1){
    setTimeout(function(){
      schedule.list(window.IDB_SCHEDULE, "protein");
      if($('#schedule-list tbody tr').length == 0)
      $('#alert-schedule-null').removeClass('hide');
    }, 2000);
  }

  $('.btn-add-term').click(function(e){

    e.preventDefault();
    $('.btn-remove-term').removeClass('hide');
    var item_number = parseInt($('#input-terms-model').find('.input-terms-number').text()) + 1;
    $('#input-terms-model').find('.input-terms-number').text(item_number);
    var input_clone = $('#input-terms-model').clone().removeClass('hide');
    input_clone.attr('id', '');
    if($('.input-terms').length == 1)
    input_clone.find('.form-group-connectors').addClass('hide');
    $('#form-group-terms .info-empty').addClass('hide');
    $('#form-group-terms').append(input_clone);
  });

  $('.btn-remove-term').click(function(e){
    e.preventDefault();
    var item_number = parseInt($('#input-terms-model').find('.input-terms-number').text()) - 1;
    $('#input-terms-model').find('.input-terms-number').text(item_number);
    if($('.input-terms').length == 2)
    $('#form-group-terms .info-empty').removeClass('hide');
    if($('.input-terms').length > 1)
    $('.input-terms').last().remove();
  });


  $(document).on('click', '.btn-key-details-show', function(e) {
    $('#modal-1').modal('show');
    var title_key = $(this).parent().find('.title-key').text();
    schedule.show(window.IDB_SCHEDULE, "protein", title_key);
  });

  $(document).on('click', '.btn-key-remove', function(e) {
    var title_key = $(this).find('.title-key').text();
    schedule.delete(window.IDB_SCHEDULE, "protein", title_key);
    $(this).parent().parent().remove();
  });

  setTimeout(function(){

    if($('#esearch-list-ids').length > 0){
      var title_key = schedule.get_last_viewed();
      var read_transition = window.IDB_SCHEDULE.transaction(["protein"], "readwrite");
      var objectStore = read_transition.objectStore("protein");
      var request = objectStore.get(title_key);
      request.onsuccess = function(event) {
        var schedule = request.result;
        $('#number-search-retmax').text(CONFIG.SEARCH_RETMAX);
        var terms_names = '';
        for(var i = 0; i < schedule.terms.length; i++){
          if(i > 0)
          terms_names+= schedule.connectors[i].value+' ';
          terms_names += schedule.terms[i].value+'['+schedule.index_fields[i].value+']';
        }
        $('#schedule-title-key').text(title_key);
        $('#terms-names').text(terms_names);
        ncbi.load_next_results(window.IDB_SCHEDULE, "protein", set_last_viewed, summary_format);
        fill_info_reports();
      }
    }
  }, 2000);

  $(document).on('click', '#load-next-results', function(event){
    ncbi.load_next_results(window.IDB_SCHEDULE, "protein", false, summary_format);
    fill_info_reports();
  });


  function fill_info_reports(){
    $(".row-search-result-new").each(function( index ) {

    });
  }

  function set_last_viewed(DB_TARGET, schedule, esearch_result_obj, esummary_result_obj){

    var item = esummary_result_obj.getElementsByTagName("eSummaryResult")[0].childNodes[1];

    var acc = item.children[1].innerHTML;
    var created_at = item.children[5].innerHTML;
    var updated_at = item.children[6].innerHTML;

    var read_transition = DB_TARGET.transaction( support_name, "readwrite");
    var objectStore = read_transition.objectStore( support_name);
    var req_schedule = objectStore.get(schedule.title_key);

    req_schedule.onsuccess = function(event) {

      var schedule_data = req_schedule.result;

      if(esearch_result_obj.esearchresult.count == 0){
        $('.result-empty').removeClass('hide');
      }

      if(schedule_data.result_count > esearch_result_obj.esearchresult.count)
      $('.result-new-event').removeClass('hide');

      schedule_data.result_count = esearch_result_obj.esearchresult.count;
      schedule_data.executed_at = new Date().toDateString();
      schedule_data.result_last_created_at = created_at;
      schedule_data.result_last_updated_at = updated_at;
      var req_schedule_update = objectStore.put(schedule_data);
      req_schedule_update.onsuccess = function(event) {

      };
    };

  }

  var summary_counter = 0;

  function summary_format(DB_TARGET, esummary_result_obj){

    var item = esummary_result_obj.getElementsByTagName("eSummaryResult")[0].childNodes[1];

    summary_counter++;

    var html_local = '<div class="row-search-result row-search-result-new">'
    +'<span class="hide row-search-result-id">'+item.children[0].innerHTML+'</span>'
    +'<div class="row"><div class="col-xs-12">'
    +summary_counter+') '+item.children[2].innerHTML+'</a></div></div>'
    +'<div class="row">'
    +'<div class="col-xs-9">'
    +'<span class="info-detail"><strong>Accession Version</strong>: '+item.children[1].innerHTML+'</span>'
    +'<br> <span class="info-detail"><strong>Created at</strong>: '+item.children[5].innerHTML+'</span>'
    +'<br> <span class="info-detail"><strong>Updated at</strong>: '+item.children[6].innerHTML+'</span>'
    +'</div>'
    +'<div class="col-xs-3 btn-result-actions">'
    +'<button class="btn btn-circle btn-default btn-result-link"> <span class="browser-link hide">/\/www.ncbi.nlm.nih.gov/protein/'+item.children[1].innerHTML+'</span> <i class="fa fa-globe"></i></button>'
    +'<button class="btn btn-circle btn-warning btn-result-share pull-right"><i class="fa fa-share-alt"></i></button></div>'
    +'</div>';

    html_local += '<div class="row list-info-reports"><div class="col-xs-12">'
    +'<ul class="list-inline"><li><a class="dblinks" href="https:www.ncbi.nlm.nih.gov/protein/'+item.children[0].innerHTML+'?report=genpept">GenPept</a></li>'
    +'<li><a class="dblinks" href="https:www.ncbi.nlm.nih.gov/protein/'+item.children[0].innerHTML+'?report=ipg">Identical items</a></li>'
    +'<li><a class="dblinks" href="https:www.ncbi.nlm.nih.gov/protein/'+item.children[0].innerHTML+'?report=fasta">FASTA</a></li>'
    +'<li><a class="dblinks" href="https:www.ncbi.nlm.nih.gov/protein/'+item.children[0].innerHTML+'?report=graph">Graphics</a></li>'
    +'</ul>'
    +'</div></div></div>';

    $('#esearch-list-ids').append(html_local);
  }

  $(document).on('click', '.btn-result-link', function(e){
    e.preventDefault();
    var url = $(this).parent().find('.browser-link').text();
    $('#modal-1').modal('show');
    $('#modal-1').find('.modal-header > h4').html('NCBI (full result)');
    $('#modal-1').find('.modal-body').html('<iframe src="'+url+'" style="height: 500px; width: 100%;"></iframe>');
    $('#btn-modal-ok').addClass('hide');
  });

  $(document).on('click', '.btn-result-share', function(e){
    e.preventDefault();
    var url = $(this).parent().find('.browser-link').text();

    window.plugins.socialsharing.share("NCBIONOFIER", "NCBIONOFIER RESULT", null, url, function(result){
    }, function(result){
    });
  });

  $(document).on('click', '#query-protein-new', function(event){

    var form_validate = $(".form-validate").validationEngine('validate');

    if(!form_validate)
    return null;

    var title_key = $('input[name="title_key"]').val();
    var days_gap = $('select[name="days_gap"]').val();
    var time = $('select[name="time"]').val();
    var support_name = $('input[name="support_name"]').val();
    var terms_array = $('input[name="terms[]"]').serializeArray();

    if(terms_array.length == 1){
      $('#form-group-terms').validationEngine('showPrompt', 'You must setup at least one term.', 'error', "topLeft");
      return null;
    }

    var connectors = $('select[name="connectors[]"]').serializeArray();
    var index_fields_array = $('select[name="index_fields[]"]').serializeArray();
    var sort_date = $('input[name="sort_date"]:checked').val();
    var free_only = $('input[name="free_only"]').is(":checked") ? 1 : 0;
    var schedule_set = schedule.set(window.IDB_SCHEDULE, support_name, title_key, terms_array, connectors, index_fields_array, days_gap, time, sort_date, free_only);

    $('#modal-1').modal('show');
    $('#modal-1').find('.modal-header').html('<h4>Search scheduled.</h4>');

    setTimeout(function(){
      schedule.show(window.IDB_SCHEDULE, support_name, title_key);
    }, 2000);

    $('#btn-modal-ok').removeClass('hide').prop('href', './new.html').text('New schedule');
    $('#btn-modal-back').removeClass('hide');
    $('#btn-modal-back').prop('href', './index.html').text('Search schedule list');
  });
};
