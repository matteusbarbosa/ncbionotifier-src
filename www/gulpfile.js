var gulp = require("gulp");
var sass = require('gulp-sass');
var pump = require('pump');
var concat = require('gulp-concat');
var minify = require('gulp-minify');

gulp.task('compress', function() {
  return gulp.src("./js/dev/core.js")
  .pipe(minify({
        ext:{
            src:'./js/dev/core.js',
            min:'.js'
        },
    }))
  .pipe(gulp.dest('./js/'));
});


gulp.task('sass:watch', function () {
  gulp.watch('./assets/css/sass/*.scss', ['sass']);
});

gulp.task('sass', function () {
  return gulp.src('./assets/css/sass/*.scss')
  .pipe(sass.sync().on('error', sass.logError))
  .pipe(gulp.dest('./assets/css'));
});
